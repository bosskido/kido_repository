function statusChangeCallback(response) {
  if (response.status === 'connected') {
    testAPI();

  } else if (response.status === 'not_authorized') {
    FB.login(function(response) {
      statusChangeCallback2(response);
    }, {scope: 'public_profile,email'});

  } else {
    alert("not connected, not logged into facebook, we don't know");
  }
}

function statusChangeCallback2(response) {
  if (response.status === 'connected') {
    testAPI();

  } else if (response.status === 'not_authorized') {
    console.log('still not authorized!');

  } else {
    alert("not connected, not logged into facebook, we don't know");
  }
}

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

function testAPI() {
  FB.api('/me', function(response) {
      console.log(response);
      facebookSignup(response);
  });
}

$(document).ready(function() {
  FB.init({
    appId      : '1706014019667658',
    xfbml      : true,
    version    : 'v2.2'
  });
  
});
function myFacebookLogin() {checkLoginState();};